import React from 'react';
import T from 'prop-types';
import Moment from 'moment';
import './SidebarGridItem.css';

/**
 * Sidebar grid item statless component, renders the sidebar item.
 * @param {object} props Props to be passed to component.
 */
const SidebarGridItem = (props) => {
  const { name, updated, location, type } = props.item;
  const date = Moment(updated); // Using moment.js to make date formatting much easier.

  return (
    <div className="SidebarGridItem">
      <div className="leftSide">
        <p className="name" title={name}>
          {name}
        </p>
        <p className="details">
          {type} {location}
        </p>
      </div>
      <div className="rightSide">
        <p>
          {date.format('MMM DD') /* Date format: short month name, and 2 digit day */}
        </p>
        <p>
          {date.format('LT') /* Time format: HH:MM AM/PM */}
        </p>
      </div>
    </div>
  );
};

SidebarGridItem.propTypes = {
  /** Item to be rendered. */
  item: T.shape({
    id: T.number,
    name: T.string,
    created: T.number,
    updated: T.number,
    location: T.string,
    type: T.string
  }).isRequired
};

export default SidebarGridItem;
