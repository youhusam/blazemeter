import React, { Component } from 'react';
import T from 'prop-types';
import SidebarGridItem from '../SidebarGridItem';
import './Sidebar.css';

/**
 * Opened Sidebar component, contains the data
 * and handles the custom filter and sorting logic.
 */
class OpenedSidebar extends Component {
  static propTypes = {
    /**  items List of the items. */
    items: T.arrayOf(T.object).isRequired,
    /** Close sidebar function. */
    closeSidebar: T.func.isRequired,
    /** Load data function for reloading. */
    loadData: T.func.isRequired,
    /** Data loading state. */
    isLoading: T.bool.isRequired,
    /** The name of the sidebar. */
    name: T.string.isRequired
  }

  static defaultState = {
    items: [],            // List of items to be displayed
    searchTerm: '',       // String to search the names of items
    ascSort: true,        // Ascending or descending sort flag
    sortEnabled: false,   // Whether or not sort is enabled
    searchItems: []       // List of searched items
  }

  constructor(props) {
    super(props);

    this.state = {
      ...OpenedSidebar.defaultState,
      items: props.items
    };

    // `this` bindings.
    this.sortFunction = this.sortFunction.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleSort = this.handleSort.bind(this);
  }

  // Refreshes the state when recieving new items (on reload).
  // And resets the ascSort value to keep the button functionality predictable.
  componentWillReceiveProps(nextProps) {
    if (this.props.isLoading) {
      this.setState({
        ...OpenedSidebar.defaultState,
        items: nextProps.items
      });
    }
  }

  /**
   * Javascript Array Sort function callback.
   * @param {string} a First item to compare.
   * @param {string} b Second item to compare.
   */
  sortFunction(a, b) {
    const { ascSort } = this.state;

    const nameA = a.name.toUpperCase(); // Ignore case
    const nameB = b.name.toUpperCase(); // Ignore case
    if (nameA < nameB)
      return ascSort ? -1 : 1; // Check asc/desc sort.
    if (nameA > nameB)
      return ascSort ? 1 : -1; // Check asc/desc sort.
    return 0;
  }

  /**
   * Event handler for the search text box.
   * Sets state.items to the filtered results according to the search term.
   * Could be expensive, maybe add debouncing.
   * @param {Event} e The event fired on text change.
   */
  handleSearchChange(e) {
    // No need to loop on elements if no search value.
    if (!e.target.value.length) {
      return this.setState({
        searchItems: [],
        searchTerm: ''
      });
    }

    this.setState({
      searchItems: this.state.items.filter(
        item => item.name.indexOf(e.target.value) >= 0
      ),
      searchTerm: e.target.value
    });
  }

  /**
   * Event handler for sorting button.
   */
  handleSort() {
    this.setState({
      ascSort: !this.state.ascSort,
      sortEnabled: true,
      items: this.state.items.sort(this.sortFunction),
      searchItems: this.state.searchItems.sort(this.sortFunction)
    });
  }

  /**
   * Sidebar Header render function to keep the code organised and clean.
   */
  renderHeader() {
    const { items, name, loadData, closeSidebar } = this.props;

    return (
      <div className="sidebarHeader">
        <h1>
          <span className="sidebarName" title={name}>{name}</span>
          &nbsp;
          <span>{items.length}</span>
        </h1>
        <div className="buttons">
          <span className="reload" onClick={loadData}>⟳</span>
          <span className="close" onClick={closeSidebar}>&times;</span>
        </div>
      </div>
    );
  }

  /**
   * Search and Sort render function.
   */
  renderSearch() {
    const { sortEnabled, ascSort } = this.state;

    return (
      <div className="searchContainer">
        <input
          type="text"
          onChange={this.handleSearchChange}
          value={this.state.searchTerm}
          placeholder={`Search ${this.props.name}`}
        />
        <div className={`sortButton ${sortEnabled && 'sortEnabled'}`} onClick={this.handleSort}>
          <span className={(sortEnabled && ascSort) ? 'hidden' : ''}>▲</span>
          <span className={(sortEnabled && !ascSort) ? 'hidden' : ''}>▼</span>
        </div>
      </div>
    );
  }

  /**
   * Items list render function.
   * @returns Items list if not loading otherwise a loading element.
   */
  renderItems() {
    const { isLoading } = this.props;
    const { items, searchItems, searchTerm } = this.state;
    const itemsToView = searchItems.length || searchTerm.length ? searchItems : items;
    const textToView = isLoading ? 'Loading...' : 'No items found.';

    return (
      <div className="items">
        {
          isLoading || !itemsToView.length ?
            <p className="errorText">{textToView}</p> :
            itemsToView.map(item => <SidebarGridItem key={item.id} item={item} />)
        }
      </div>
    );
  }

  render() {
    return (
      <div className="OpenedSidebar">
        {this.renderHeader()}
        {this.renderSearch()}
        {this.renderItems()}
      </div>
    );
  }
}

export default OpenedSidebar;
