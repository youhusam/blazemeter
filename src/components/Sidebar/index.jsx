import React, { Component } from 'react';
import T from 'prop-types';
import OpenedSidebar from './OpenedSidebar';
import ClosedSidebar from './ClosedSidebar';
import './Sidebar.css';

/**
 * Main Sidebar Component. Takes name as a prop.
 */
class Sidebar extends Component {
  static propTypes = {
    /** The name of the sidebar. */
    name: T.string.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      opened: false,      // Whether sidebar is opened.
      data: [],           // List of data.
      isLoading: true,    // If it's fetching data.
      closing: false      // Used to trigger closing animation. Or use react-transition-group instead.
    };

    // `this` bindings because js.
    this.loadData = this.loadData.bind(this);
    this.openSidebar = this.openSidebar.bind(this);
    this.closeSidebar = this.closeSidebar.bind(this);
  }

  // Load data after component is mounted.
  componentDidMount() {
    this.loadData();
  }

  /**
   * Fetches the data from the server, sorts it by updated
   * and loading it in the state.
   * It clears the current data before it fetches.
   */
  loadData() {
    const that = this;
    this.setState({ isLoading: true, data: [] });
    fetch('/sidebar.json')
      .then(data => data.json())
      .then(json => {
        that.setState({ data: json.sort((a,b) => a.updated - b.updated), isLoading: false });
      })
      .catch(() => { that.setState({ isLoading: false }); });
  }

  /**
   * Opens the sidebar.
   */
  openSidebar() {
    this.setState({ closing: true });
    setTimeout(() => {
      this.setState({ opened: true, closing: false });
    }, 150);
  }

  /**
   * Triggers closing animation and closes the sidebar.
   */
  closeSidebar() {
    this.setState({ closing: true });
    setTimeout(() => {
      this.setState({ opened: false, closing: false });
    }, 300);
  }

  render() {
    const sidebarClassName = this.state.opened ? // Nested Ternary...  baad
      this.state.closing ? 'opened closing' : 'opened' :
      this.state.closing ? 'closed closing'  : 'closed';

    return (
      <div className={`Sidebar ${sidebarClassName}`}>
        {
          this.state.opened ?
            <OpenedSidebar
              items={this.state.data}
              closeSidebar={this.closeSidebar}
              loadData={this.loadData}
              isLoading={this.state.isLoading}
              name={this.props.name}
            /> :
            <ClosedSidebar
              name={this.props.name}
              openSidebar={this.openSidebar}
            />
        }
      </div>
    );
  }
}

export default Sidebar;
