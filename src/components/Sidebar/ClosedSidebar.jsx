import React from 'react';
import T from 'prop-types';
import './Sidebar.css';

/**
 * Closed sidebar component, renders a button that opens the sidebar on click.
 * @param {object} props Props to be passed to component.
 */
const ClosedSidebar = props => (
  <div className="openButton" onClick={props.openSidebar} title={props.name}>
    <span role="img" aria-label="Open">
      ⚠
    </span>
  </div>
);

ClosedSidebar.propTypes = {
  /** Function to open the sidebar when clicking on the component. */
  openSidebar: T.func.isRequired,
  name: T.string.isRequired
};

export default ClosedSidebar;
