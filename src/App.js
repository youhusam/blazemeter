import React from 'react';
import Sidebar from './components/Sidebar';
import './App.css';

const App = () => (
  <div className="App">
    <Sidebar name="Reports" />
  </div>
);

export default App;
